import hf.Hf;
import bugfix.Bugfix;
import debug.Debug;
import game_params.GameParams;
import merlin.Merlin;
import vault.Vault;
import patchman.IPatch;
import patchman.Patchman;

import kavo.Kavo;

@:build(patchman.Build.di())
class Main {
  public static function main(): Void {
    Patchman.bootstrap(Main);
  }

  public function new(
    debug: Debug,
    bugfix: Bugfix,
    game_params: GameParams,
    kavo: Kavo,
    vault: Vault,
    merlin: Merlin,
    patches: Array<IPatch>,
    hf: Hf
  ) {
    Patchman.patchAll(patches, hf);
  }
}
