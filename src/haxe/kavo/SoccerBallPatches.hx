package kavo;

import etwin.ds.FrozenArray;
import patchman.IPatch;
import etwin.Obfu;
import patchman.Ref;
import custom_clips.CustomClips;
import merlin.Merlin;
import merlin.value.MerlinFloat;

@:build(patchman.Build.di())
class SoccerBallPatches {

  private static var GOAL_VAR: String = Obfu.raw("SOCCER_GOALS");

  @:diExport
  public var patches(default, null): FrozenArray<IPatch>;
  
  public function new() {
    this.patches = FrozenArray.of(
      // Gives points to the kicking player and record goal.
      Ref.auto(hf.entity.bomb.player.SoccerBall.infix).after(function(hf, self) {
        var field = self.world.getCase({x: self.cx, y: self.cy});
        if (field != hf.Data.FIELD_GOAL_1 && field != hf.Data.FIELD_GOAL_2)
          return;
        if (self.lastPlayer == null)
          return;

        self.lastPlayer.getScore(self, Std.random(20)*50 + 1000);
        var goals = Merlin.getCurLevelVar(self.game, GOAL_VAR).or(new MerlinFloat(0)).unwrapFloat();
        Merlin.setCurLevelVar(self.game, GOAL_VAR, new MerlinFloat((goals: Float) + 1));
      }),
      // Make soccerball affected by white bombs.
      Ref.auto(hf.entity.bomb.player.Classic.onExplode).after(function(hf, self) {
        explosion(hf, self, 0.8);
      })
    );
  }

  // Adapted from RepelBomb.explode.
  public static function explosion(hf: hf.Hf, self: hf.entity.Bomb, coefficient: Float): Void {
    for(ball in self.game.getList(hf.Data.SOCCERBALL)) {
      var ball: hf.entity.bomb.player.SoccerBall = cast ball;
      var d = self.distance(ball.x, ball.y);

      if (d <= self.radius) {
        var coeff = (self.radius - d) / self.radius;
        var angle = Math.atan2(ball.y - self.y, ball.x - self.x);

        ball.dy = -self.power * coeff;
        if (!ball.fl_stable) {
          ball.dy *= 1.5;
        }
        ball.dx = Math.cos(angle) * self.power * coeff * coefficient;

        ball.burn();

        if (ball.fl_stable) {
          ball.dx *= 2;
        } else {
          ball.dx *= 1.5;
        }
      }
    }
  }
}
