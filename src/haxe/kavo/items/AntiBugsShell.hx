package kavo.items;

import etwin.Obfu;
import vault.IItem;
import merlin.Merlin;
import merlin.value.MerlinBool;

import etwin.flash.MovieClip;
import hf.Hf;
import hf.entity.Item;
import hf.SpecialManager;

class AntiBugsShell implements IItem {

  private static inline var MARIO_MODE_ID: Int = 114;
  private static inline var SCORE_VALUE: Int = 47500;
  private static inline var SCRIPT_VAR: String = "marioDone";
  
  public var id(default, null): Int = 118;
  public var sprite(default, null): Null<String> = null;

  public function new() {
  }

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
    specMan.player.getScore(item, SCORE_VALUE);
    Merlin.setGlobalVar(specMan.game, SCRIPT_VAR, new MerlinBool(true));
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {
  }

  public function skin(mc: MovieClip, subId: Null<Int>): Void {
    mc.gotoAndStop(MARIO_MODE_ID + 1);
  }
}
