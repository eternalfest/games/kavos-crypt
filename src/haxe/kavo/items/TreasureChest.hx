package kavo.items;

import etwin.Obfu;
import vault.IItem;

import etwin.flash.MovieClip;
import hf.Hf;
import hf.entity.Item;
import hf.SpecialManager;

class TreasureChest implements IItem {

  private static inline var ANARCHIPEL_ID: Int = 20;
  
  public var id(default, null): Int = 120;
  public var sprite(default, null): Null<String> = null;

  public function new() {
  }

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
    for (_ in 0...5) {
      var e = hf.entity.item.ScoreItem.attach(specMan.game, item.x, item.y, 51, hf.Std.random(4));
      e.moveFrom(item, 8);
    }
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {
  }

  public function skin(mc: MovieClip, subId: Null<Int>): Void {
    mc.gotoAndStop(ANARCHIPEL_ID + 1);
  }
}
