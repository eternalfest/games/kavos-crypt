package kavo.items;

import etwin.Obfu;
import vault.IItem;

import etwin.flash.MovieClip;
import hf.Hf;
import hf.entity.Item;
import hf.SpecialManager;

class OneUp implements IItem {

  private static inline var FIGO_ID: Int = 16;
  
  public var id(default, null): Int = 119;
  public var sprite(default, null): Null<String> = null;

  public function new() {
  }

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
    ++specMan.player.lives;
    specMan.game.gi.setLives(specMan.player.pid, specMan.player.lives);
    specMan.game.fxMan.attachShine(item.x, item.y - hf.Data.CASE_HEIGHT * 0.5);
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {
  }

  public function skin(mc: MovieClip, subId: Null<Int>): Void {
    mc.gotoAndStop(FIGO_ID + 1);
  }
}
