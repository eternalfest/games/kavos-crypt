package kavo.actions;

import etwin.Obfu;
import merlin.IAction;
import merlin.IActionContext;

import custom_clips.CustomClips;

class KillStrike implements IAction {

  public var name(default, null): String = Obfu.raw("killStrike");
  public var isVerbose(default, null): Bool = false;

  private var customClips: CustomClips;

  public function new(customClips: CustomClips) {
    this.customClips = customClips;
  }

  public function run(ctx: IActionContext): Bool {
    var game = ctx.getGame();
    var hf = ctx.getHf();
    var x = game.flipCoordCase(ctx.getInt(Obfu.raw("x")));
    kavo.entities.KillStrike.attach(game, this.customClips, (x + 0.5) * hf.Data.CASE_WIDTH);
    return false;
  }
}
