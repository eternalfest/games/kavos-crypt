package kavo.actions;

import etwin.Obfu;
import merlin.IAction;
import merlin.IActionContext;

class Darkness2 implements IAction {

  public var name(default, null): String = Obfu.raw("darkness2");
  public var isVerbose(default, null): Bool = false;

  public function new() {
  }

  public function run(ctx: IActionContext): Bool {
    var v = ctx.getOptFloat(Obfu.raw("v"));
    var halo = ctx.getOptFloat(Obfu.raw("halo"));
    var t = ctx.getOptFloat(Obfu.raw("t")).toNullable();

    var game = ctx.getGame();

    v.map(v => game.forcedDarkness = v);
    halo.map(halo => {
      DarknessHaloPatches.setHalo(game, halo, t);
    });
    game.updateDarkness();

    return false;
  }
}
