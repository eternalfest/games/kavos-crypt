package kavo.actions;

import etwin.Obfu;
import merlin.IAction;
import merlin.IActionContext;

import custom_clips.CustomClips;

class ClearSoccerBalls implements IAction {

  public var name(default, null): String = Obfu.raw("clearSoccerBalls");
  public var isVerbose(default, null): Bool = false;

  public function new() {
  }

  public function run(ctx: IActionContext): Bool {
    var game = ctx.getGame();
    var hf = ctx.getHf();

    for (ball in game.getList(hf.Data.SOCCERBALL)) {
      game.fxMan.attachFx(ball.x, ball.y, "hammer_fx_pop");
      ball.destroy();
    }
    return false;
  }
}
