package kavo.actions;

import etwin.Obfu;
import merlin.IAction;
import merlin.IActionContext;

import custom_clips.CustomClips;

class Mc2 implements IAction {

  public var name(default, null): String = Obfu.raw("mc2");
  public var isVerbose(default, null): Bool = false;

  public function new() {
  }

  public function run(ctx: IActionContext): Bool {
    var scaleX = ctx.getOptFloat(Obfu.raw("sx"));
    var scaleY = ctx.getOptFloat(Obfu.raw("sy"));
    var rotation = ctx.getOptFloat(Obfu.raw("rot"));
    var frame = ctx.getOptInt(Obfu.raw("f"));

    merlin.Actions.MC.run(ctx);

    var game = ctx.getGame();
    var spriteList = game.world.view.mcList;
    var sprite = spriteList[spriteList.length - 1];

    scaleY.map(sy => sprite._yscale = sy);
    scaleX.map(sx => sprite._xscale *= sx / 100); // preserve flip

    rotation.map(rot => sprite._rotation = rot);
    frame.map(f => {
      sprite.gotoAndStop(f);
      untyped sprite.sub.stop();
    });
    return false;
  }
}
