package kavo.actions;

import etwin.Obfu;
import merlin.IAction;
import merlin.IActionContext;

class Disguise implements IAction {

  public var name(default, null): String = Obfu.raw("disguise");
  public var isVerbose(default, null): Bool = false;

  public function new() {
  }

  public function run(ctx: IActionContext): Bool {
    var game = ctx.getGame();

    var freeze = ctx.getOptBool(Obfu.raw("freeze"));
    var hat = ctx.getOptInt(Obfu.raw("hat"));
    
    freeze.map(freeze => game.fl_disguise = freeze);
    hat.map(hat => for (p in game.getPlayerList()) {
      var old = p.head;
      p.head = hat;
      if (hat != old)
        p.replayAnim();
    });

    return false;
  }
}
