package kavo.actions;

import hf.mode.GameMode;

import etwin.Obfu;
import merlin.IAction;
import merlin.IActionContext;

class SolidTile implements IAction {

  public var name(default, null): String = Obfu.raw("solidTile");
  public var isVerbose(default, null): Bool = false;

  public function new() {
  }

  public function run(ctx: IActionContext): Bool {
    var game = ctx.getGame();

    var x1 = Std.int(game.flipCoordCase(ctx.getInt(Obfu.raw("x1"))));
    var x2 = Std.int(game.flipCoordCase(ctx.getInt(Obfu.raw("x2"))));
    var y1 = ctx.getInt(Obfu.raw("y1"));
    var y2 = ctx.getInt(Obfu.raw("y2"));

    while (x1 != x2 || y1 != y2) {
      forceEntitiesUp(game, x1, y1);
      if (x1 < x2) x1++ else if (x1 > x2) x1--;
      if (y1 < y2) y1++ else if (y1 > y2) y1--;
    }
    forceEntitiesUp(game, x1, y1);

    return false;
  }

  private static function forceEntitiesUp(game: GameMode, cx: Int, cy: Int): Void {
    var hf = game.root;

    var entities = [];
    for (e in game.world.triggers[cx][cy]) {
      if (e.cx != cx || e.cy != cy)
        continue;
      entities.push(e);
    }

    for (e in entities) {
      e.moveTo(e.x, hf.Entity.y_ctr(cy - 1));
    }
  }
}
