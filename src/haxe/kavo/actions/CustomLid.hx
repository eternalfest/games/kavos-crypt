package kavo.actions;

import etwin.Obfu;
import merlin.IAction;
import merlin.IActionContext;

class CustomLid implements IAction {

  public var name(default, null): String = Obfu.raw("customLid");
  public var isVerbose(default, null): Bool = false;

  private static var LIDS: Array<String> = [
    "µ°±", "5$Í", "¢tb", "‰#w", "©e¿!", "fdH!",
    "'(jn", "^-xê", "S3jo", ";lKd", "çZ'à", "Fi%N",
  ];

  public function new() {
  }

  public function run(ctx: IActionContext): Bool {
    var game = ctx.getGame();
    var name = null;
    ctx.getOptInt(Obfu.raw("id")).map(id => {
      name = LIDS[id - 1];
    });

    if (name == null)
      name = ctx.getArg(Obfu.raw("txt")).unwrapString();

    game.fakeLevelId = null;
    game.gi.level.text = name;
    game.gi.level.textColor = game.gi.baseColor;
    return false;
  }
}
