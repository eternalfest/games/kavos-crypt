package kavo.actions;

import etwin.Obfu;
import merlin.IAction;
import merlin.IActionContext;

class McEachTile implements IAction {

  public var name(default, null): String = Obfu.raw("mcEachTile");
  public var isVerbose(default, null): Bool = false;

  public function new() {
  }

  public function run(ctx: IActionContext): Bool {
    var sid = ctx.getInt(Obfu.raw("sid"));
    var name = ctx.getString(Obfu.raw("n"));
    var back = ctx.getOptBool(Obfu.raw("back")).or(false);
    var dx = ctx.getOptInt(Obfu.raw("dx")).or(0);
    var dy = ctx.getOptInt(Obfu.raw("dy")).or(0);

    var hf = ctx.getHf();
    var game = ctx.getGame();
    for (x in 0...hf.Data.LEVEL_WIDTH) {
      var xr = game.flipCoordReal(hf.Entity.x_ctr(x) + dx);
      if (game.fl_mirror)
        xr += hf.Data.CASE_WIDTH;

      for (y in 0...hf.Data.LEVEL_HEIGHT) {
        if (game.world.getCase({ x: x, y: y }) <= 0)
          continue;

        var yr = hf.Entity.y_ctr(y) + dy;

        ctx.killById(sid);

        var mc = game.world.view.attachSprite(name, xr, yr, back);
        if (mc == null) {
          patchman.DebugConsole.warn("BadMcLink: " + name);
          return false;
        }

        mc.stop();
        ctx.registerMc(sid, mc);
        sid++;
      }
    }

    return false;
  }
}
