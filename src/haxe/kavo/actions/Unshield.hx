package kavo.actions;

import etwin.Obfu;
import merlin.IAction;
import merlin.IActionContext;

class Unshield implements IAction {

  public var name(default, null): String = Obfu.raw("unshield");
  public var isVerbose(default, null): Bool = false;

  public function new() {
  }

  public function run(ctx: IActionContext): Bool {
    var game = ctx.getGame();
    for (p in game.getPlayerList()) {
      p.unshield();
    }
    return false;
  }
}
