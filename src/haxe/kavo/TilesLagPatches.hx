package kavo;

import etwin.ds.FrozenArray;
import etwin.ds.WeakMap;
import etwin.ds.Nil;
import etwin.flash.MovieClip;
import hf.native.BitmapData;
import patchman.IPatch;
import patchman.Ref;

@:build(patchman.Build.di())
class TilesLagPatches {

  private static inline var MARIO_DIM_DID: Int = 1;

  @:diExport
  public var patches(default, null): FrozenArray<IPatch>;
  
  public function new() {
    this.patches = FrozenArray.of(
      Ref.auto(hf.levels.View.display).before(function(hf, self, id) {
        // Don't use tile cache if we are in dim Mario.
        if (self.world.game.currentDim == MARIO_DIM_DID) {
          self.fl_cache = false;
        }
      })
    );
  }
}
