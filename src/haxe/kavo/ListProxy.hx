package kavo;

import etwin.ds.Nil;
import etwin.Obfu;

import merlin.IRefFactory;
import merlin.IProxy;
import merlin.value.MerlinValue;
import merlin.value.MerlinFloat;
import merlin.value.MerlinObject;
import merlin.value.MerlinFunction;
import merlin.refs.FrozenFactory;

class ListProxy implements IProxy {

  public static var LIST_REF(default, null): IRefFactory = new FrozenFactory(
    Obfu.raw("list"),
    MerlinFunction.fromFunc(makeList)
  );

  public var values: Array<MerlinValue>;
  
  public function new(values: Array<MerlinValue>) {
    this.values = values;
  }

  public function has(key: String): Bool {
    if (key == "length")
      return true;
    var i = Std.parseInt(key);
    return i != null && i >= 0 && i < this.values.length;
  }

  public function get(key: String): Nil<MerlinValue> {
    if (key == "length")
      return (new MerlinFloat(this.values.length): MerlinValue);
    var i = Std.parseInt(key);
    if (i != null)
      return this.values[i];
    return Nil.none();
  }

  public function set(key: String, value: MerlinValue): Void {
    var i = Std.parseInt(key);
    if (i == null)
      throw new merlin.ScriptError("Can't modify field " + key + " on list");
    this.values[i] = value;
  }

  public static function makeList(args: Array<MerlinValue>): MerlinObject {
    return MerlinObject.proxy(new ListProxy(args));
  }
}
