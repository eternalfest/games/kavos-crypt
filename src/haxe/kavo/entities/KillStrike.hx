package kavo.entities;

import custom_clips.CustomClips;

@:hfTemplate
class KillStrike extends hf.Entity {

  private static inline var MC_NAME: String = "hammer_fx_death";
  private static inline var SOUND_NAME: String = "sound_bad_death";
  private static inline var NB_COPIES: Int = 5;
  private static inline var RADIUS: Float = 12;
  private static inline var DEADLY_TIME: Float = 7;
  private static inline var STRIKE_DELAY: Float = 15;

  private var extraCopies: Int = NB_COPIES;
  private var strikeDelay: Float = STRIKE_DELAY;
  private var deadlyTimer: Float = DEADLY_TIME;
  
  private function new() {
    super(null);
  }

  public static function attach(g: hf.mode.GameMode, customClips: CustomClips, x: Float): KillStrike {
    var hf = g.root;
    var sp = customClips.attach(g.depthMan, MC_NAME, hf.Data.DP_FX, KillStrike.getClass(hf));
    sp.extraCopies--;
    sp.init(g);
    sp.register(hf.Data.SUPA);
    sp.x = x;
    sp.y = hf.Data.GAME_HEIGHT * 0.5;
    sp.stop();

    return sp;
  }

  public override function update(): Void {
    var hf = this.game.root;

    if (this.strikeDelay > 0) {
      this.strikeDelay -= hf.Timer.tmod;
      if (this.strikeDelay > 0)
        return;
      
      this.play();
      this.game.soundMan.playSound(SOUND_NAME, hf.Data.CHAN_BAD);
      this.game.shake(10, 3);

    } else if (this.extraCopies > 0) {
      this.extraCopies--;
      for (_ in 0...2) {
        var mc = this.game.depthMan.attach(MC_NAME, hf.Data.DP_FX);
        mc._x = this.x;
        mc._y = this.y;
      }
    }

    if (this.deadlyTimer > 0) {
      this.deadlyTimer -= hf.Timer.tmod;
      for (p in this.game.getPlayerList()) {
        if (Math.abs(p.x - this.x) <= RADIUS)
          p.killHit(null);
      }
    }
  }
}
