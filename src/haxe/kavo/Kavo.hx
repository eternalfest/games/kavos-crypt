package kavo;

import etwin.ds.FrozenArray;
import patchman.IPatch;
import custom_clips.CustomClips;
import merlin.IAction;
import merlin.IRefFactory;
import vault.IItem;

@:build(patchman.Build.di())
class Kavo {

  @:diExport
  public var actions(default, null): FrozenArray<IAction>;

  @:diExport
  public var patches(default, null): FrozenArray<IPatch>;

  @:diExport
  public var merlinRefs(default, null): FrozenArray<IRefFactory>;

  @:diExport
  public var items(default, null): FrozenArray<IItem>;
  
  public function new(
    customClips: CustomClips,
    soccerBallPatches: SoccerBallPatches,
    darknessHaloPatches: DarknessHaloPatches,
    tilesLagPatches: TilesLagPatches,
    noNextLevel: better_script.NoNextLevel,
    deadlyBottom: better_script.DeadlyBottom,
    patches: Array<IPatch>,
    merlinRefs: Array<IRefFactory>
  ) {
    this.patches = FrozenArray.uncheckedFrom(patches);

    this.actions = FrozenArray.of(
      new kavo.actions.KillStrike(customClips),
      new kavo.actions.CustomLid(),
      new kavo.actions.ClearSoccerBalls(),
      new kavo.actions.Mc2(),
      new kavo.actions.McEachTile(),
      new kavo.actions.Disguise(),
      new kavo.actions.Darkness2(),
      new kavo.actions.SolidTile(),
      new kavo.actions.Unshield()
    );

    merlinRefs.push(ListProxy.LIST_REF);
    this.merlinRefs = FrozenArray.uncheckedFrom(merlinRefs);

    this.items = FrozenArray.of(
      new kavo.items.AntiBugsShell(),
      new kavo.items.OneUp(),
      new kavo.items.TreasureChest()
    );
  }
}
