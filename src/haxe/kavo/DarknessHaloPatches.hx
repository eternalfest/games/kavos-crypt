package kavo;

import etwin.ds.FrozenArray;
import etwin.ds.WeakMap;
import patchman.IPatch;
import etwin.Obfu;
import patchman.Ref;

import hf.mode.GameMode;

@:build(patchman.Build.di())
class DarknessHaloPatches {

  private static var HALOES: WeakMap<GameMode, HaloInfo> = new WeakMap();
  private static inline var HALO_DEFAULT_SIZE: Int = 100;

  @:diExport
  public var patches(default, null): FrozenArray<IPatch>;
  
  public function new() {
    this.patches = FrozenArray.of(
      Ref.auto(GameMode.initLevel).before(function(hf, self) {
        HALOES.remove(self);
      }),
      Ref.auto(GameMode.holeUpdate).after(function(hf, self) {
        var halo = HALOES.get(self);
        if (halo == null || self.darknessMC._name == null)
          return;

        var incr = halo.increment * hf.Timer.tmod;
        if (incr == null) {
          halo.curSize = halo.targetSize;
        } else {
          halo.curSize += incr;
          if (incr > 0 ? (halo.curSize >= halo.targetSize) : (halo.curSize <= halo.targetSize)) {
            halo.curSize = halo.targetSize;
            halo.increment = null;
          }
        }

        var players = self.getPlayerList();
        for (i in 0...players.length) {
          var hole = self.darknessMC.holes[i];
          hole._xscale = halo.curSize;
          hole._yscale = halo.curSize;
          if (players[i].fl_candle || players[i].specialMan.actives[68]) {
            hole._xscale *= 1.5;
            hole._yscale *= 1.5;
          }
        }
      })
    );
  }

  public static function setHalo(game: GameMode, size: Float, time: Null<Float>): Void {
    var halo = HALOES.get(game);
    if (halo == null) {
      halo = {
        curSize: HALO_DEFAULT_SIZE,
        targetSize: HALO_DEFAULT_SIZE,
        increment: null,
      };
      HALOES.set(game, halo);
    }

    halo.targetSize = size;
    halo.increment = time == null ? null : (size - halo.curSize) / time;
  }

}

private typedef HaloInfo = {
  curSize: Float,
  targetSize: Float,
  increment: Null<Float>,
};